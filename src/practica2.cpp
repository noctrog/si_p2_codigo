#include <pipeline.h>

int main(int argc, char *argv[])
{
    Pipeline pipe("./scenes/snap_0point.pcd", "./objects");

    // Eliminar los planos dominantes de la escena
    pipe.eliminarPlanosDominantes(0.05);
    pipe.eliminarPlanosDominantes(0.04);
    pipe.eliminarPlanosDominantes(0.02);

    // Mostrar la escena con los planos dominantes quitados
    pipe.mostrarEscena();

    // Extraer los puntos característicos de todas las nubes
    pipe.extraerTodosPuntosCaracteristicos<Pipeline::SUSANKeypoints>();

    // Extraer los descriptores de todas las nubes
    pipe.extraerTodosLosDescriptores<Pipeline::Descriptor>();

    // RANSAC
    pipe.ransacPrerejective();

    // Debug: visualizar Ransac
    pipe.visualizarResultadosRansac();

    // ICP
    pipe.refinarPosiciones();

    // Visualizar resultado
    pipe.visualizarResultados();

    pcl::console::print_highlight("Se ha tardado %f segundos", pipe.segundosTotales());

    return 0;
}
