#include <iostream>
#include <chrono>
#include <string>

#include <pipeline.h>

#include <pcl/io/pcd_io.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/ia_ransac.h>
#include <pcl/registration/sample_consensus_prerejective.h>
#include <pcl/registration/icp.h>

#include <thread>

Pipeline::Pipeline(const std::string_view ruta_escena,
        const std::string_view carpeta_objetos)
    : escena{new Nube}, tiempo_total_{ 0.0f }
{
    // Cargar la nube de puntos de la escena
    if (pcl::io::loadPCDFile<Punto>(ruta_escena.data(), *escena) < 0) {
        std::cerr << "Error al cargar la nube de puntos de la escena" << std::endl;
        exit(-1);
    } else {
        // Mantener una copia para mostrar al final
        escena_original = Nube::Ptr {new Nube};
        pcl::copyPointCloud(*escena, *escena_original);
    }

    /// Cargar todas las nubes de los objetos
    // Rutas a todos los objetos
    const std::vector<std::string> rutas_objetos {"./objects/s0_mug_corr.pcd",
                                            "./objects/s0_piggybank_corr.pcd",
                                            "./objects/s0_plant_corr.pcd",
                                            "./objects/s0_plc_corr.pcd"};

    for (const auto& ruta : rutas_objetos) {
        objetos.emplace_back(new Nube);

        // Cargar nube de puntos del objeto actual
        if (pcl::io::loadPCDFile<Punto>(ruta, *objetos[objetos.size()-1]) < 0) {
            std::cerr << "Error al cargar la nube de puntos del objeto" << std::endl;
            exit(-1);
        }
    }

    // Reservar memoria para los futuros indices de los keypoints de los objetos
    indices_objetos.reserve(objetos.size());
    // Reservar memoria para los futuros descriptores de los objetos
    descriptores_objetos.reserve(objetos.size());
}

Pipeline::~Pipeline(void)
{

}

// Mostrar nube
void Pipeline::mostrarEscena(void)
{
    using namespace pcl::visualization;
    auto viewer = std::make_shared<PCLVisualizer>("Visualizador 3D");
    viewer->addCoordinateSystem (1.0);
    PointCloudColorHandlerRGBField<Punto> rgb(escena);
    viewer->addPointCloud<Punto> (escena, rgb, "id0");
    while(!viewer->wasStopped ()) {
        viewer->spinOnce(100);
        std::this_thread::sleep_for(std::chrono::microseconds(100000));
    }

    viewer->close();
}

void Pipeline::eliminarPlanosDominantes(const float umbral=0.03)
{
    // Coeficientes del modelo, y puntos inliers que representan los planos
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
    // Crea el objeto de segmentación
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Parámetros opcionales
    seg.setOptimizeCoefficients (true);
    // Parámetros obligatorios
    seg.setModelType (pcl::SACMODEL_PLANE); // Se busca un plano
    seg.setMethodType (pcl::SAC_RANSAC);    // Algoritmo de RANSAC
    seg.setDistanceThreshold (umbral);        // Distancia umbral

    // Nube en la que encontrar los planos, tiene que ser XYZ
    pcl::PointCloud<pcl::PointXYZ>::Ptr nube_xyz(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::copyPointCloud(*escena, *nube_xyz);
    seg.setInputCloud(nube_xyz);

    // Ejecutar algoritmo
    seg.segment(*inliers, *coefficients);

    // Eliminar los puntos obtenidos como pertenecientes a planos 
    pcl::ExtractIndices<pcl::PointXYZRGBA> extractor;
    extractor.setInputCloud(escena);
    extractor.setIndices(inliers);
    extractor.setNegative(true);
    extractor.filter(*escena);
}

pcl::PointCloud<pcl::Normal>::Ptr Pipeline::calcularNormales(
        pcl::PointCloud<Punto>::Ptr nube)
{
    // Crear nube de normales
    pcl::PointCloud<pcl::Normal>::Ptr normales (new pcl::PointCloud<pcl::Normal>());

    // Instanciar la clase estimadora de normales y pasarle la nube
    pcl::NormalEstimationOMP<Punto, pcl::Normal> estimador;
    estimador.setInputCloud(nube);

    // Crear un kdtree que será usado por el estimador para acelerar la búsqueda de los puntos
    // vecinos
    pcl::search::KdTree<Punto>::Ptr kd_tree (new pcl::search::KdTree<Punto>());
    estimador.setSearchMethod(kd_tree);
    
    // Parámetros del estimador
    estimador.setRadiusSearch(0.03);    // Usar todos los vecinos dentro de un radio

    // Calcular las normales
    estimador.compute(*normales);

    // Comprobar el tamaño de la nube resultante con la original, deben de tener el mismo tamaño
    if (normales->points.size() != nube->points.size())
        pcl::console::print_error("ERROR: La nube de normales calculada no tiene el mismo tamano que la nube original (%u y %u)\n", 
                                  nube->points.size(), normales->points.size());

    // Devolver la nube resultante
    return normales;
}

// pair<escena, objeto>
pcl::CorrespondencesPtr Pipeline::extraerCorrespondenciasObjeto(NubeDesc::Ptr objeto, pcl::PointIndicesConstPtr indices_objeto)
{
    // Vector para guardar las correspondencias
    pcl::CorrespondencesPtr correspondencias(new pcl::Correspondences);

    // Usar un kdtree para acelerar la búsqueda de los keypoints similares
    pcl::KdTreeFLANN<Descriptor> descriptor_kdtree;
    descriptor_kdtree.setInputCloud(descriptores_escena);

    // Resultados de kNN
    const int k = 1;
    std::vector<int> k_indices(k);
    std::vector<float> k_distancias_cuadradas(k);
    // Por cada punto en los descriptores del objeto, buscar su correspondencia más parecida en la escana
    for (size_t i = 0; i < objeto->points.size(); ++i) {
        descriptor_kdtree.nearestKSearch(*objeto, i, k, k_indices, k_distancias_cuadradas);
        correspondencias->emplace_back(pcl::Correspondence(k_indices[0], i, k_distancias_cuadradas[0]));
    }

    return correspondencias;
}

void Pipeline::calcularEmparejamientos(void)
{
    // Reservar memoria para las correspondencias
    todas_correspondencias.clear();
    todas_correspondencias.reserve(descriptores_objetos.size());

    // Calcular los emparejamientos por cada objeto
    for (size_t i = 0; i < descriptores_objetos.size(); ++i) {
        todas_correspondencias.emplace_back(extraerCorrespondenciasObjeto(
                    descriptores_objetos[i],
                    indices_objetos[i]
                    ));
    }

    pcl::console::print_highlight("Todos las correspondencias calculadas. Numero de correspondencias del primer objeto: %d\n", todas_correspondencias[0]->size());
}

void Pipeline::rechazarEmparejamientos(void)
{
    // TODO
    for (size_t i = 0; i < todas_correspondencias.size(); ++i) {
        // Las correspondencias que se van a tratar en esta iteracion
        pcl::CorrespondencesPtr correspondencias = todas_correspondencias[i];

        pcl::console::print_highlight("Nube de objeto tiene %d puntos\n",
                keypoints_objetos[i]->points.size());
        pcl::console::print_highlight("Nube de escena tiene %d puntos\n",
                keypoints_escena->points.size());

    }

    //pcl::console::print_highlight("Correspondencias finales del primer objeto: %d\n", todas_correspondencias[0]->size());
}

void Pipeline::ransacPrerejective(void)
{
    pcl::StopWatch watch;

    // Reservar memoria para las nubes alineadas
    keypoints_objetos_alineados.clear();
    keypoints_objetos_alineados.reserve(objetos.size());
    // y para las transformaciones
    transformaciones_objetos.clear();
    transformaciones_objetos.reserve(objetos.size());
    // y para los errores
    errores_euclideos_ransac.clear();
    errores_euclideos_ransac.reserve(objetos.size());

    // Por cada uno de los objetos que se quieren encontrar en la escena
    for (size_t i = 0; i < objetos.size(); ++i) {
        // Nube de puntos para guardar la nube una vez sea alineada
        Nube::Ptr objeto_alineado { new Nube };
        // RANSAC para calcular la alineación inicial a la vez que se eliminan las correspondencias no consistentes
        pcl::SampleConsensusPrerejective<Punto, Punto, Descriptor> ransac;
        ransac.setInputSource(keypoints_objetos[i]);
        ransac.setInputTarget(keypoints_escena);
        ransac.setSourceFeatures(descriptores_objetos[i]);
        ransac.setTargetFeatures(descriptores_escena);

        // En vez de coger directamente el vecino más cercano, se coge aleatoriamente de los n
        // vecinos más cercanos indicados en el parámetro. Esto lo hace robusto a outliers, a costa
        // de incrementar el tiempo de cálculo
        ransac.setCorrespondenceRandomness(2);
        // El porcentaje de puntos que tienen que convertirse en inliers para dar por válida una
        // transformación
        ransac.setInlierFraction(0.5);
        // El número de muestras para cada iteración (mínimo 3 para 6 DOF)
        ransac.setNumberOfSamples(3);
        // Coeficiente de similaridad entre las longitudes de las aristas (0-1). Cuanto más cerca de 1,
        // más estricto será, con la posibilidad de rechazar poses aceptables
        ransac.setSimilarityThreshold(0.99);
        // Máxima distancia que puede haber entre dos puntos correspondientes, si se supera, esa
        // iteración se ignora
        ransac.setMaxCorrespondenceDistance(0.01);
        // Número máximo de iteraciones
        ransac.setMaximumIterations(37000);
        
        // Ejecutar RANSAC
        ransac.align(*objeto_alineado);

        // Guardar error euclídeo
        errores_euclideos_ransac.emplace_back(ransac.getFitnessScore());
        
        // Si RANSAC ha convergido, se obtiene la matriz de transformación correspondiente
        if (ransac.hasConverged()) {
            keypoints_objetos_alineados.push_back(objeto_alineado);
            transformaciones_objetos.push_back(ransac.getFinalTransformation());
        } else {
            // No se guardan los resultados
            keypoints_objetos_alineados.emplace_back(nullptr);
            transformaciones_objetos.emplace_back(Eigen::Matrix4f::Identity());

            pcl::console::print_warn("No se ha encontrado el objeto %d\n", i);
        }

        std::cout << "Matriz de transformación del objeto " << i << ":\n" << 
            transformaciones_objetos[i] << std::endl;
    }

    tiempo_total_ += watch.getTimeSeconds();
}

void Pipeline::refinarPosiciones(void)
{
    pcl::StopWatch watch;
    // Reservar la memoria necesaria
    keypoints_objetos_refinados.clear();
    keypoints_objetos_refinados.reserve(objetos.size());
    transformaciones_objetos_refinadas.clear();
    transformaciones_objetos_refinadas.reserve(objetos.size());
    objetos_resultados.clear();
    objetos_resultados.reserve(objetos.size());
    errores_euclideos_icp.clear();
    errores_euclideos_icp.reserve(objetos.size());

    // Para cada objeto a refinar
    for (size_t i = 0; i < objetos.size(); ++i) {
        // Si RANSAC no ha podido calcular la alineación inicial, pasar al siguiente objeto
        if (keypoints_objetos_alineados[i] == nullptr) {
            // No guardar la transformación ni la nube
            keypoints_objetos_refinados.emplace_back(Nube::Ptr {new Nube});
            transformaciones_objetos_refinadas.emplace_back(Eigen::Matrix4f::Identity());
            objetos_resultados.emplace_back(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr {new pcl::PointCloud<pcl::PointXYZRGBA>});
            continue;
        }

        // Nube para guardar los resultados
        Nube::Ptr resultado {new Nube};

        // Nube de entrada (nube con todos los puntos, transformada)
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr objeto_alineado { new pcl::PointCloud<pcl::PointXYZRGBA> };
        pcl::transformPointCloud(*objetos[i], *objeto_alineado, transformaciones_objetos[i]);

        // Clase ICP para ejecutar el algoritmo
        pcl::IterativeClosestPoint<pcl::PointXYZRGBA, pcl::PointXYZRGBA> icp;

        // Objeto a alinear
        icp.setInputCloud(objeto_alineado);
        // Escena
        icp.setInputTarget(escena);
        // Número máximo de iteraciones
        icp.setMaximumIterations(100);
        // La diferencia mínima entre la última transformación y la recién calculada (criterio de finalización)
        icp.setTransformationEpsilon(1e-9);
        // Parar cuando la suma de los errores de posición al cuadrado sea menor que n
        icp.setEuclideanFitnessEpsilon(1);
        // Correspondencias con distancias mayores serán ignoradas
        icp.setMaxCorrespondenceDistance(0.05);

        icp.align(*resultado);

        // Guardar el error
        errores_euclideos_icp.emplace_back(icp.getFitnessScore());

        if (icp.hasConverged()) {
            // Aplicar la transformación y guardar la nube
            transformaciones_objetos_refinadas.emplace_back(icp.getFinalTransformation());
            objetos_resultados.push_back(resultado);

            std::cout << "Transformacion calculada por ICP en el objeto " << i << ":" << std::endl << 
                transformaciones_objetos_refinadas.back() << std::endl;
        } else {
            // No guardar la transformación ni la nube
            keypoints_objetos_refinados.emplace_back(Nube::Ptr {new Nube});
            transformaciones_objetos_refinadas.emplace_back(Eigen::Matrix4f::Identity());
            objetos_resultados.emplace_back(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr {new pcl::PointCloud<pcl::PointXYZRGBA>});

            pcl::console::print_warn("No se ha podido refinar el objeto %d\n", i);
        }
    }

    tiempo_total_ += watch.getTimeSeconds();
}

float Pipeline::segundosTotales(void)
{
   return tiempo_total_;
}

void Pipeline::visualizarResultadosRansac(void)
{
    using namespace pcl::visualization;

    // Visualizador para mostrar las nubes por pantalla
    auto viewer = std::make_shared<pcl::visualization::PCLVisualizer>("Visualizador 3D");
    // Sistema de coordenadas con escala unitaria
    viewer->addCoordinateSystem (1.0);

    // Mostrar la escena
    PointCloudColorHandlerRGBField<Punto> rgb{ escena_original };
    viewer->addPointCloud<Punto>(escena_original, rgb, "Escena");

    // Aplicar la transformación a todas las nubes y mostrarlas
    size_t i = 0;
    for (const auto& transformacion : transformaciones_objetos) {
        // Aplicar la transformación (Si RANSAC no ha podido calcularla, será la identidad)
        Nube::Ptr n_nube {new Nube};
        pcl::transformPointCloud(*objetos[i], *n_nube, transformacion);
        // Colorear los objetos de color verde para distinguirlos
        PointCloudColorHandlerCustom<Punto> rgb(n_nube, 0, 255, 0);
        // Añadir la nube del objeto en el viewport
        viewer->addPointCloud<Punto>(n_nube, rgb, std::to_string(i));
        ++i;
    }

    pcl::console::print_info("Errores euclideos RANSAC: ");
    for (auto error : errores_euclideos_ransac) {
        std::cout << error << " ";
    }
    std::cout << std::endl;

    while(!viewer->wasStopped ()) {
        viewer->spinOnce(100);
        std::this_thread::sleep_for(std::chrono::microseconds(100000));
    }

    viewer->close();
}

void Pipeline::visualizarResultados(void)
{
    using namespace pcl::visualization;

    // Visualizador para mostrar las nubes por pantalla
    auto viewer = std::make_shared<pcl::visualization::PCLVisualizer>("Visualizador 3D");
    // Sistema de coordenadas con escala unitaria
    viewer->addCoordinateSystem (1.0);

    // Mostrar la escena
    PointCloudColorHandlerRGBField<Punto> rgb{ escena_original };
    viewer->addPointCloud<Punto>(escena_original, rgb, "Escena");

    // Mostrar los objetos encontrados en ella
    size_t i = 0;
    for (const auto& objeto : objetos_resultados) {
        // Si el objeto no ha sido obtenido, pasar al siguiente
        if (!objeto) continue;

        // Colorear los objetos de color verde para distinguirlos
        PointCloudColorHandlerCustom<Punto> rgb(objeto, 0, 255, 0);
        // Añadir la nube del objeto en el viewport
        viewer->addPointCloud<Punto>(objeto, rgb, std::to_string(i));
        ++i;
    }

    pcl::console::print_info("Errores euclideos ICP: ");
    for (auto error : errores_euclideos_icp) {
        std::cout << error << " ";
    }
    std::cout << std::endl;

    while(!viewer->wasStopped ()) {
        viewer->spinOnce(100);
        std::this_thread::sleep_for(std::chrono::microseconds(100000));
    }

    viewer->close();
}
