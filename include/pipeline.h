#pragma once

#include <string_view>
#include <vector>
#include <any>

#include <pcl/common/common.h>
#include <pcl/registration/correspondence_types.h>
#include <pcl/point_types.h>
#include <pcl/keypoints/harris_3d.h>
#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/keypoints/susan.h>
#include <pcl/filters/uniform_sampling.h>
#include <pcl/features/fpfh_omp.h>
#include <pcl/features/shot_omp.h>
#include <pcl/features/rsd.h>
#include <pcl/features/3dsc.h>
#include <pcl/features/usc.h>
#include <pcl/features/vfh.h>
#include <pcl/features/esf.h>
#include <pcl/point_types_conversion.h>
#include <pcl/features/intensity_gradient.h>
#include <pcl/features/rift.h>

class Pipeline
{
    public:
        using Punto = pcl::PointXYZRGBA;
        using Nube = pcl::PointCloud<Punto>;
        // Keypoints
        using HARRISKeypoints = pcl::HarrisKeypoint3D<pcl::PointXYZI, pcl::PointXYZI>;
        using SIFTKeypoints   = pcl::SIFTKeypoint<Punto, Punto>;
        using USKeypoints     = pcl::UniformSampling<Punto>;
        using SUSANKeypoints  = pcl::SUSANKeypoint<Punto, Punto>;

        // Descriptores
        using FPFHDescriptor  = pcl::FPFHSignature33;
        using SHOTDescriptor  = pcl::SHOT1344;
        using RSDDescriptor   = pcl::PrincipalRadiiRSD;
        using SC3DDescriptor  = pcl::ShapeContext1980;
        using USCDescriptor   = pcl::UniqueShapeContext1960;

        // Descriptores globales NO SE USAN
        using VFHDescriptor   = pcl::VFHSignature308;
        using ESFDescriptor   = pcl::ESFSignature640;
        using RIFTDescriptor  = pcl::Histogram<32>;

        using Descriptor = USCDescriptor;
        using NubeDesc = pcl::PointCloud<Descriptor>;
        Pipeline(const std::string_view ruta_escena,
                const std::string_view carpeta_objetos);
        ~Pipeline ();

        // Visualizar la escena
        void mostrarEscena(void);
        // Eliminar los planos dominantes
        void eliminarPlanosDominantes(const float umbral);
        // Extraer todos los puntos característicos
        template<typename T>
        void extraerTodosPuntosCaracteristicos(void);
        // Extraer los descriptores de la escena y los objetos
        template<typename T>
        void extraerTodosLosDescriptores(void);
        // Calcular los emparejamientos de los objetos con la escena
        void calcularEmparejamientos(void);
        // Rechazar los emparejamientos no validos
        void rechazarEmparejamientos(void);
        // RANSAC que se encarga de realizar el rechazo de emparejamientos malos a la vez
        void ransacPrerejective(void);
        // Refinamiento de las posiciones calculadas con ICP
        void refinarPosiciones(void);
        // Visualizar los resultados de las transformaciones calculadas por RANSAC
        void visualizarResultadosRansac(void);
        // Visualizar los resultados finales
        void visualizarResultados(void);
        // Devuelve el tiempo tardado en ejecutar el pipeline
        float segundosTotales(void);
    private:
        // Extraer puntos caracteristicos de la escena
        template<typename T>
            void extraerPuntosCaracteristicosEscena(void);
        // Extraer puntos caracteristicos de los objetos
        template<typename T>
            void extraerPuntosCaracteristicosObjetos(void);
        // Extrae los puntos de una nube
        template<typename T>
            std::pair<pcl::PointIndicesConstPtr, Nube::Ptr>
            extraerPuntosCaracteristicos(Nube::Ptr nube);
        // Calcular las normales de los puntos de una nube
        pcl::PointCloud<pcl::Normal>::Ptr calcularNormales(Nube::Ptr nube);
        // Extraer los descriptores de los puntos caracteristicos
        template<typename T>
            auto extraerDescriptores(Nube::Ptr nube, Nube::Ptr nube_kp);
        // Comparar la escena con un solo objeto, std::pair<escena, objeto>
        pcl::CorrespondencesPtr extraerCorrespondenciasObjeto(NubeDesc::Ptr objeto, pcl::PointIndicesConstPtr indices_objeto);

        // Datos
        Nube::Ptr escena_original;
        Nube::Ptr escena;
        std::vector<Nube::Ptr> objetos;
        // Puntos característicos
        pcl::PointIndicesConstPtr indices_escena;
        std::vector<pcl::PointIndicesConstPtr> indices_objetos;
        Nube::Ptr keypoints_escena;
        std::vector<Nube::Ptr> keypoints_objetos;
        // Descriptores
        NubeDesc::Ptr descriptores_escena;
        std::vector<NubeDesc::Ptr> descriptores_objetos;
        // Correspondencias
        std::vector<pcl::CorrespondencesPtr> todas_correspondencias;
        // Nubes alineadas
        std::vector<Nube::Ptr> keypoints_objetos_alineados;
        // Matrices de transformación para cada objeto (antes de ICP)
        std::vector<Eigen::Matrix4f> transformaciones_objetos;
        // Errores euclideos RANSAC (antes de ICP)
        std::vector<double> errores_euclideos_ransac;
        // Nubes refinadas
        std::vector<Nube::Ptr> keypoints_objetos_refinados;
        // Matrices de transformación refinadas
        std::vector<Eigen::Matrix4f> transformaciones_objetos_refinadas;
        // Errores de las nubes refinadas
        std::vector<double> errores_euclideos_icp;
        // Nubes resultantes para cada objeto
        std::vector<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr> objetos_resultados;
        // Tiempo total que se ha tardado en ejecutar el programa
        float tiempo_total_;
};

template<typename T>
void Pipeline::extraerTodosPuntosCaracteristicos(void)
{
    pcl::StopWatch watch;
    extraerPuntosCaracteristicosEscena<T>();
    extraerPuntosCaracteristicosObjetos<T>();

    tiempo_total_ += watch.getTimeSeconds();
}

    template<typename T>
void Pipeline::extraerPuntosCaracteristicosObjetos(void)
{
    indices_objetos.resize(objetos.size());
    keypoints_objetos.resize(objetos.size());
    size_t i = 0;

    for (const auto& nube : objetos) {
        // Extraer los indices de los puntos característicos
        std::pair<pcl::PointIndicesConstPtr, Nube::Ptr> resultado 
            = extraerPuntosCaracteristicos<T>(nube);

        // Guardar los resultados
        indices_objetos[i] = std::get<0>(resultado);
        keypoints_objetos[i] = std::get<1>(resultado);

        // Extraer los indices de los puntos caracteristicos
        //indices_objetos.emplace_back(extraerPuntosCaracteristicos(nube));                

        // Generar la nube de puntos característicos, extrayendo los índices contrarios a los keypoints
        //keypoints_objetos[i] = Nube::Ptr(new Nube);
        //pcl::ExtractIndices<pcl::PointXYZRGBA> filtro(true);
        //filtro.setInputCloud(nube);
        //filtro.setIndices(indices_objetos.back());
        //filtro.filter(*keypoints_objetos[i]);
        ++i;
    }
}

    template<typename T>
void Pipeline::extraerPuntosCaracteristicosEscena(void)
{
    // Extraer los indices de los puntos característicos
    std::pair<pcl::PointIndicesConstPtr, Nube::Ptr> resultado 
        = extraerPuntosCaracteristicos<T>(escena);

    // Guardar los resultados
    indices_escena = std::get<0>(resultado);
    keypoints_escena = std::get<1>(resultado);

    // Generar la nube de puntos característicos, extrayendo los índices contrarios a los keypoints
    //keypoints_escena = Nube::Ptr(new Nube);
    //pcl::ExtractIndices<pcl::PointXYZRGBA> filtro(true);
    //filtro.setInputCloud(escena);
    //filtro.setIndices(indices_escena);
    //filtro.filter(*keypoints_escena);
}

template<typename T>
std::pair<pcl::PointIndicesConstPtr, pcl::PointCloud<Pipeline::Punto>::Ptr>
Pipeline::extraerPuntosCaracteristicos(Nube::Ptr nube)
{
    if constexpr(std::is_same_v<T, HARRISKeypoints>) {
        // Detector de puntos Harris
        HARRISKeypoints detector;
        // Keypoints resultantes después de la operación
        pcl::PointCloud<Pipeline::Punto>::Ptr keypoints { new pcl::PointCloud<Pipeline::Punto> };
        pcl::PointCloud<pcl::PointXYZI>::Ptr keypoints_i { new pcl::PointCloud<pcl::PointXYZI> };

        // TODO: Se necesita pasar la nube a puntos con intensidad
        pcl::PointCloud<pcl::PointXYZI>::Ptr aux { new pcl::PointCloud<pcl::PointXYZI> };
        pcl::copyPointCloud(*nube, *aux);

        // Parámetros del algoritmo HARRIS
        detector.setNonMaxSupression(true);
        detector.setInputCloud(aux);
        detector.setThreshold (1e-5);

        // Reloj
        pcl::StopWatch watch;

        // Calcular HARRIS
        detector.compute(*keypoints_i);

        // Log
        pcl::console::print_highlight ("Se han detectado %zd puntos como keypoints en %lfs\n", keypoints->size (), watch.getTimeSeconds ());

        // Guardar los índices de la nube de entrada que han resultado ser keypoints
        pcl::PointIndicesConstPtr indices = detector.getKeypointsIndices();
        if (indices->indices.empty())
            pcl::console::print_warn ("No se ha encontrado ningun keypoint!\n");

        // Conseguir los puntos XYZRGBA a partir de los indices
        pcl::ExtractIndices<pcl::PointXYZRGBA> filtro(false);
        filtro.setInputCloud(nube);
        filtro.setIndices(indices);
        filtro.filter(*keypoints);

        return {indices, keypoints};
    }

    if constexpr(std::is_same_v<T, SIFTKeypoints>) {
        // Detector de puntos SIFT
        SIFTKeypoints detector;
        //T detector;

        // Nube de keypoints resultantes después de la operación
        Nube::Ptr keypoints (new Nube);

        // Parámetros para SIFT
        const float min_scale = 0.01f;
        const int n_octaves = 6;
        const int n_scales_per_octave = 10;
        const float min_contrast = 0.05f;

        // Preparar el algorimto SIFT
        pcl::PointCloud<pcl::PointWithScale> result;
        pcl::search::KdTree<Punto>::Ptr tree(new pcl::search::KdTree<Punto>());
        detector.setSearchMethod(tree);
        detector.setScales(min_scale, n_octaves, n_scales_per_octave);
        detector.setMinimumContrast(min_contrast);
        detector.setInputCloud(nube);

        // Reloj
        pcl::StopWatch watch;

        // Se calculan los keypoints
        detector.compute(*keypoints);

        // Log
        pcl::console::print_highlight ("Se han detectado %zd puntos como keypoints en %lfs\n", keypoints->size (), watch.getTimeSeconds ());

        // Pasar ambas nubes a XYZ para compararlas con un kdtree y obtener los indices
        pcl::PointCloud<pcl::PointXYZ>::Ptr kp_xyz { new pcl::PointCloud<pcl::PointXYZ> };
        pcl::PointCloud<pcl::PointXYZ>::Ptr nube_xyz { new pcl::PointCloud<pcl::PointXYZ> };
        pcl::copyPointCloud(*keypoints, *kp_xyz);
        pcl::copyPointCloud(*nube, *nube_xyz);

        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree_nube { new pcl::search::KdTree<pcl::PointXYZ>() };
        tree_nube->setInputCloud(nube_xyz);
        pcl::PointIndicesPtr indices { new pcl::PointIndices };
        // Guardar los índices de la nube de entrada que han resultado ser keypoints
        // Por cada uno de los keypoints calculados, encontrar el más cercano en la nube original
        const int k = 1;    // Número de vecinos más cercanos a buscar
        std::vector<int> k_indices(k);  // Indice del punto más cercano
        std::vector<float> k_dist(k);   // Distancia al cuadrado de ese punto
        for (int i = 0; i < kp_xyz->points.size(); ++i) {
            tree_nube->nearestKSearch(*kp_xyz, i, k, k_indices, k_dist);
            indices->indices.push_back(i);
        }

        if (indices->indices.size() == 0) {
            pcl::console::print_warn("SIFT no ha encontrado\n");
        }

        return {indices, keypoints};
    }

    if constexpr (std::is_same_v<T, USKeypoints>) {
        pcl::StopWatch watch;
        Nube::Ptr keypoints (new Nube);
        pcl::UniformSampling<Punto> detector;
        detector.setInputCloud(nube);
        detector.setRadiusSearch(0.02);
        detector.filter(*keypoints);

        // Log
        pcl::console::print_highlight ("Se han detectado %zd puntos como keypoints en %lfs con US\n", keypoints->size (), watch.getTimeSeconds ());

        return {{}, keypoints};
    }

    if constexpr (std::is_same_v<T, SUSANKeypoints>) {
        auto normales = calcularNormales(nube);
        pcl::StopWatch watch;
        Nube::Ptr keypoints (new Nube);
        pcl::SUSANKeypoint<Punto, Punto> detector;
        detector.setInputCloud(nube);
        //detector.setDistanceThreshold(0.02);
        //detector.setGeometricValidation(true);
        detector.setNormals(normales);

        detector.compute(*keypoints);

        return {{}, keypoints};
    }
}

    template <typename T>
void Pipeline::extraerTodosLosDescriptores(void)
{
    pcl::StopWatch watch;

    // Calcular los descriptores de la escena
    descriptores_escena = extraerDescriptores<T>(escena, keypoints_escena);

    // Calcular los descriptores de todos los objetos
    size_t i = 0;
    for (const auto& objeto : objetos) {
        descriptores_objetos.emplace_back(extraerDescriptores<T>(objeto, keypoints_objetos[i]));

        pcl::console::print_highlight("Número de descriptores del objeto %d: %d\n", i, descriptores_objetos.back()->size());

        ++i;
    }

    // Log
    pcl::console::print_highlight("Numero de descriptores de escena calculados: %u\n", descriptores_escena->size());

    tiempo_total_ += watch.getTimeSeconds();
}

    template<typename T>
auto Pipeline::extraerDescriptores(Nube::Ptr nube, Nube::Ptr nube_kp)   
{
    if constexpr (std::is_same_v<T, FPFHDescriptor>) {
        // Calcular las normales de la nube
        auto normales = calcularNormales(nube);

        // Instanciar la clase de descriptor de puntos y especificar las entradas
        pcl::FPFHEstimationOMP<Punto, pcl::Normal, T> fpfh;
        fpfh.setInputCloud(nube_kp);
        fpfh.setInputNormals(normales);
        fpfh.setSearchSurface(nube);

        // Crear un KdTree para simplificar la búsqueda de los puntos más cercanos
        pcl::search::KdTree<Punto>::Ptr kd_tree (new pcl::search::KdTree<Punto>);
        fpfh.setSearchMethod(kd_tree);

        // Descriptores a calcular
        typename pcl::PointCloud<T>::Ptr resultados (new pcl::PointCloud<T>());

        // Parámetros del algoritmo
        fpfh.setRadiusSearch(0.05);     // Importante: El radio de búsqueda TIENE que ser MAYOR que el radio utilizado para calcular las normales

        // Calcular los descriptores
        fpfh.compute(*resultados);

        // Devolver los resultados
        return resultados;
    }

    if constexpr (std::is_same_v<T, SHOTDescriptor>) {
        // Calcular las normales
        auto normales = calcularNormales(nube);

        // Instanciar la clase de descriptor de puntos y especificar las entradas
        pcl::SHOTColorEstimationOMP<Punto, pcl::Normal, T> shot;
        shot.setInputCloud(nube_kp);
        shot.setInputNormals(normales);
        shot.setSearchSurface(nube);
        shot.setRadiusSearch(0.02);

        // Descriptores a calcular
        typename pcl::PointCloud<T>::Ptr resultados (new pcl::PointCloud<T>());

        // Parámetros del algoritmo

        // Calcular los descriptores
        shot.compute(*resultados);

        // Devolver los resultados
        return resultados;
    }

    if constexpr (std::is_same_v<T, RSDDescriptor>) {
        // Calcular las normales
        auto normales = calcularNormales(nube);
        pcl::RSDEstimation<Punto, pcl::Normal, pcl::PrincipalRadiiRSD> rsd;
        rsd.setInputCloud(nube_kp);
        rsd.setInputNormals(normales);
        rsd.setSearchSurface(nube);
        // Radio de búsqueda. IMPORTANTE: tiene que ser mayor que el radio usado para estimar las
        // normales
        rsd.setRadiusSearch(0.05);
        // Radio mayor que el cual será considerado un plano
        rsd.setPlaneRadius(0.1);
        // ¿Se quiere guardar el histograma completo de distancia-angulo?
        rsd.setSaveHistograms(false);
        // Kdtree para que las búsquedas sean mucho más rápidas
        pcl::search::KdTree<Punto>::Ptr kdtree(new pcl::search::KdTree<Punto>);
        // Nube en la que guardar los resultados
        typename pcl::PointCloud<T>::Ptr resultados (new pcl::PointCloud<T>());

        rsd.setSearchMethod(kdtree);
        rsd.compute(*resultados);

        return resultados;
    }

    if constexpr (std::is_same_v<T, SC3DDescriptor>) {
        // Calcular las normales
        auto normales = calcularNormales(nube);

        pcl::ShapeContext3DEstimation<Punto, pcl::Normal, pcl::ShapeContext1980> sc3d;
        sc3d.setInputCloud(nube_kp);
        sc3d.setInputNormals(normales);
        sc3d.setSearchSurface(nube);
        // Kdtree para que las búsquedas sean mucho más rápidas
        pcl::search::KdTree<Punto>::Ptr kdtree(new pcl::search::KdTree<Punto>);
        sc3d.setSearchMethod(kdtree);
        // Radio en el que buscar vecinos. También será el radio de la esfera de soporte
        sc3d.setRadiusSearch(0.05);
        // El radio minimo de la esfera, para evitar ser demasiado sensible en contenedores cercanos
        // al centro de la misma
        sc3d.setMinimalRadius(0.05 / 10.0);
        // El radio usado para calcular la densidad local de puntos de los puntos vecinos
        sc3d.setPointDensityRadius(0.05 / 5.0);

        // Nube en la que guardar los resultados
        typename pcl::PointCloud<T>::Ptr resultados (new pcl::PointCloud<T>());
        sc3d.compute(*resultados);

        return resultados;
    }

    if constexpr (std::is_same_v<T, USCDescriptor>) {
        // USC estimation object.
        pcl::UniqueShapeContext<Punto, T, pcl::ReferenceFrame> usc;
        usc.setInputCloud(nube_kp);
        usc.setSearchSurface(nube);
        // Radio de búsqueda para encontrar vecinos, tambien es el radio de la esfera de soporte
        usc.setRadiusSearch(0.05);
        // Radio minimo de la esfera, para evitar ser demasiado sensible en puntos cercanos al
        // centro.
        usc.setMinimalRadius(0.05 / 10.0);
        // Radio usado para computar la densidad local
        usc.setPointDensityRadius(0.05 / 5.0);
        // Radio para computar la Local Reference Frame
        usc.setLocalRadius(0.05);

        // Nube en la que guardar los resultados
        typename pcl::PointCloud<T>::Ptr resultados (new pcl::PointCloud<T>());
        usc.compute(*resultados);

        return resultados;
    }

    if constexpr (std::is_same_v<T, VFHDescriptor>) {
        auto normales = calcularNormales(nube);
        // VFH estimation object.
        pcl::VFHEstimation<Punto, pcl::Normal, T> vfh;
        vfh.setInputCloud(nube_kp);
        vfh.setSearchSurface(nube);
        vfh.setInputNormals(normales);
        pcl::search::KdTree<Punto>::Ptr kdtree(new pcl::search::KdTree<Punto>);
        vfh.setSearchMethod(kdtree);
        // Optionally, we can normalize the bins of the resulting histogram,
        // using the total number of points.
        vfh.setNormalizeBins(true);
        // Also, we can normalize the SDC with the maximum size found between
        // the centroid and any of the cluster's points.
        vfh.setNormalizeDistance(false);

        typename pcl::PointCloud<T>::Ptr resultados (new pcl::PointCloud<T>());
        vfh.compute(*resultados);

        return resultados;
    }

    if constexpr (std::is_same_v<T, ESFDescriptor>) {
        pcl::ESFEstimation<Punto, T> esf;
        esf.setInputCloud(nube_kp);
        esf.setSearchSurface(nube);

        typename pcl::PointCloud<T>::Ptr resultados (new pcl::PointCloud<T>());
        esf.compute(*resultados);

        return resultados;
    }

    if constexpr (std::is_same_v<T, RIFTDescriptor>) {
        auto normales = calcularNormales(nube);

        // Convertir el color a intensidad
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr nube_aux { new pcl::PointCloud<pcl::PointXYZRGB> };
        pcl::PointCloud<pcl::PointXYZI>::Ptr nube_intensidad { new pcl::PointCloud<pcl::PointXYZI> };
        pcl::PointCloud<pcl::PointXYZI>::Ptr nube_kp_intensidad { new pcl::PointCloud<pcl::PointXYZI> };
        pcl::copyPointCloud(*nube, *nube_aux);
        pcl::PointCloudXYZRGBtoXYZI(*nube_aux, *nube_intensidad);
        pcl::copyPointCloud(*nube_kp, *nube_aux);
        pcl::PointCloudXYZRGBtoXYZI(*nube_aux, *nube_kp_intensidad);

        // Calcular la intensidad de los gradientes
        pcl::PointCloud<pcl::IntensityGradient>::Ptr gradientes { new pcl::PointCloud<pcl::IntensityGradient> };
        pcl::IntensityGradientEstimation <pcl::PointXYZI, pcl::Normal, pcl::IntensityGradient,
            pcl::common::IntensityFieldAccessor<pcl::PointXYZI> > ge;
        ge.setInputCloud(nube_kp_intensidad);
        ge.setSearchSurface(nube_intensidad);
        ge.setInputNormals(normales);
        ge.setRadiusSearch(0.03);
        ge.compute(*gradientes);

        // Estimar los descriptores RIFT
        pcl::RIFTEstimation<pcl::PointXYZI, pcl::IntensityGradient, T> rift;
        rift.setInputCloud(nube_kp_intensidad);
        pcl::search::KdTree<pcl::PointXYZI>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZI>);
        rift.setSearchMethod(kdtree);
        // Set the intensity gradients to use.
        rift.setInputGradient(gradientes);
        // Radius, to get all neighbors within.
        rift.setRadiusSearch(0.02);
        // Set the number of bins to use in the distance dimension.
        rift.setNrDistanceBins(4);
        // Set the number of bins to use in the gradient orientation dimension.
        rift.setNrGradientBins(8);
        // Note: you must change the output histogram size to reflect the previous values.

        typename pcl::PointCloud<T>::Ptr resultados (new pcl::PointCloud<T>());
        rift.compute(*resultados);

        return resultados;
    }
}

